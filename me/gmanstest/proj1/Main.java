package me.gmanstest.proj1;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.FileConfiguration;


import java.util.Arrays;
import java.util.Random;

public class Proj1 extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
         this.saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);
        // Plugin startup logic
        autobroadcast();

        } //HAD TO CLOSE THIS

    public void autobroadcast() {
        final String[] messages = {
                ChatColor.GOLD +  getConfig().getString("message"),
                ChatColor.YELLOW + "Test2 In Yellow",
                ChatColor.BLUE + "Test3 In Blue"
        };
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                Bukkit.broadcastMessage(Arrays.asList(messages).get(new Random().nextInt(messages.length)));
            }
        }, 0, 200); //ticks
    }

}